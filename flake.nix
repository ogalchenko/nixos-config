{
  description = "Localhost RDE";

  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-unstable";
    };

    nixpkgs-stable = {
      url = "github:nixos/nixpkgs/nixos-21.11";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    NUR = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-stable, home-manager, NUR, fenix, ... }@inputs:
    let
      # findModules and pkgsFor copy-pasted from github:balsoft/nixos-config
      findModules = dir:
        builtins.concatLists (builtins.attrValues (builtins.mapAttrs
          (name: type:
            if type == "regular" then [{
              name = builtins.elemAt (builtins.match "(.*)\\.nix" name) 0;
              value = dir + "/${name}";
            }] else if (builtins.readDir (dir + "/${name}"))
              ? "default.nix" then [{
              inherit name;
              value = dir + "/${name}";
            }] else
              findModules (dir + "/${name}"))
          (builtins.readDir dir)));

      pkgsFor = system:
        import inputs.nixpkgs {
          overlays = [ self.nixosOverlays fenix.overlays.default ];
          localSystem = { inherit system; };
          config = {
            allowUnfree = true;
          };
        };
    in
    {
      nixosProfiles = builtins.listToAttrs (findModules ./profiles);

      nixosOverlays = import ./overlay.nix inputs;

      nixosConfigurations = with nixpkgs.lib;
        let
          hosts = builtins.attrNames (builtins.readDir ./hosts);

          mkHost = name:
            let
              system = builtins.readFile (./hosts + "/${name}/system");
              pkgs = pkgsFor system;
              packages.${system}.default = fenix.packages.${system}.default.toolchain;
            in
            nixosSystem {
              inherit system;
              modules = [
                (import (./hosts + "/${name}"))
                { nixpkgs.pkgs = pkgs; }
                inputs.nixpkgs.nixosModules.notDetected
              ];
              specialArgs = { inherit inputs; };
            };

        in
        genAttrs hosts mkHost;
    };

}
