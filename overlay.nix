inputs: final: prev:
let
  
  old = import inputs.nixpkgs ({
    localSystem = {
      inherit (final) system;
    };
    config.allowUnfree = true;
  });

  inherit (final) system lib;

in rec {

  nur = (import inputs.NUR {
    pkgs = old;
    nurpkgs = final;
  }).repos;
}
