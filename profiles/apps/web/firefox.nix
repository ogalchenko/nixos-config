{ inputs, config, pkgs, ... }:
{

  environment.sessionVariables = {
    MOZ_USE_XINPUT2 = "1";
    MOZ_DBUS_REMOTE = "1";
  };


  home-manager.users.alex = {

    programs.firefox = {
      enable = true;

      profiles.default = {
        id = 0;
        name = "Default";
        isDefault = true;
        userChrome = ''
        
          '';
        settings = {
          "toolkit.telemetry.archive.enabled" = false;
          "toolkit.telemetry.bhrPing.enabled" = false;
          "toolkit.telemetry.enabled" = false;
          "toolkit.telemetry.firstShutdownPing.enabled" = false;
          "toolkit.telemetry.hybridContent.enabled" = false;
          "toolkit.telemetry.newProfilePing.enabled" = false;
          "toolkit.telemetry.reportingpolicy.firstRun" = false;
          "toolkit.telemetry.shutdownPingSender.enabled" = false;
          "toolkit.telemetry.unified" = false;
          "toolkit.telemetry.updatePing.enabled" = false;
          "toolkit.telemetry.coverage.opt-out" = true;
          "toolkit.coverage.opt-out" = true;
          "toolkit.coverage.endpoint.base" = "";
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

          "browser.search.defaultenginename" = "DuckDuckGo";
          "browser.search.selectedEngine" = "DuckDuckGo";
          "browser.urlbar.placeholderName" = "DuckDuckGo";
          "browser.search.region" = "US";
          "browser.contentblocking.category" = "strict";
          "browser.ping-centre.telemetry" = false;
          "browser.compactmode.show" = true;
          "browser.newtabpage.activity-stream.feeds.telemetry" = false;
          "browser.newtabpage.activity-stream.telemetry" = false;
          "browser.newtabpage.activity-stream.feeds.snippets" = false;
          "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
          "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
          "browser.newtabpage.activity-stream.showSponsored" = false;
          "browser.newtabpage.activity-stream.feeds.discoverystreamfeed" = false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
          "browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts" = false;
          "browser.startup.page" = "3";
          "browser.region.network.url" = "";
          "browser.region.update.enabled" = false;
          "browser.download.useDownloadDir" = false;
          "browser.download.alwaysOpenPanel" = false;
          "browser.download.manager.addToRecentDocs" = false;
          "browser.startup.blankWindow" = false;
          "browser.display.use_system_colors" = false;
          "browser.toolbars.bookmarks.visibility" = "never";

          "geo.provider.network.url" = "https://location.services.mozilla.com/v1/geolocate?key=%MOZILLA_API_KEY%";
          "geo.provider.ms-windows-location" = false;
          "geo.provider.use_corelocation" = false;
          "geo.provider.use_gpsd" = false;

          "network.cookie.thirdparty.sessionOnly" = true;
          "network.cookie.thirdparty.nonsecureSessionOnly" = true;
          "network.gio.supported-protocols" = "";
          "network.trr.mode" = "2"; # Enable DNS via HTTPS
          "network.trr.uri" = "https://dns.quad9.net/dns-query";
          "network.trr.confirmationNS" = "skip";
          "network.dns.disablePrefetch" = true;

          "privacy.window.maxInnerWidth" = 1600;
          "privacy.window.maxInnerHeight" = 900;
          "privacy.donottrackheader.enabled" = true;
          "privacy.trackingprotection.enabled" = true;
          "privacy.trackingprotection.socialtracking.enabled" = true;
          "privacy.partition.network_state.ocsp_cache" = true;
          "privacy.userContext.enabled" = true;
          "privacy.userContext.ui.enabled" = true;
          "privacy.partition.serviceWorkers" = true;
          "privacy.resistFingerprinting" = true;
          "privacy.resistFingerprinting.block_mozAddonManager" = true;
          "privacy.resistFingerprinting.letterboxing" = false;

          "datareporting.policy.dataSubmissionEnabled" = false;
          "datareporting.healthreport.uploadEnabled" = false;

          "security.ssl.require_safe_negotiation" = true;
          "security.tls.enable_0rtt_data" = false;
          "security.mixed_content.block_display_content" = true;

          "fission.autostart" = true;
          "permissions.manager.defaultsUrl" = "";
          "permissions.delegation.enabled" = false;

          "dom.security.https_only_mode" = "true";
          "dom.security.https_only_mode_ever_enabled" = true;
          "dom.disable_open_during_load" = true;
          "webgl.disabled" = true;
          "dom.disable_window_move_resize" = true;

          "experiments.activeExperiment" = false;
          "experiments.enabled" = false;
          "experiments.supported" = false;
          "network.allow-experiments" = false;
          "gfx.font_rendering.opentype_svg.enabled" = false;
          # WebRTC
          "media.peerconnection.enabled" = false;

          "extensions.activeThemeID" = "firefox-compact-light@mozilla.org";
        };
      };

      extensions = with pkgs.nur.rycee.firefox-addons; [
        ublock-origin
        grammarly
        darkreader
        search-by-image
        foxyproxy-standard
      ];

    };

  };
}
