{ inputs, config, pkgs, ... }:
{

  home-manager.users.alex = {

    programs.tmux = {
      enable = true;
    };

    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";

      enableAutosuggestions = true;
      enableCompletion = true;
      defaultKeymap = "emacs";

      oh-my-zsh = {
        enable = true;
        plugins = [ ];
        theme = "alanpeabody";
      };

      history = {
        path = ".config/zsh/zsh_history";
        size = 10000;
        save = 10000;
        ignoreDups = true;
        expireDuplicatesFirst = true;
        extended = true;
        share = true;
      };

      shellAliases = {
        ls = "ls -al --color";
      };

    };
  };

}
