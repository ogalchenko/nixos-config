{ lib, pkgs, ... }: {
  boot = {
    loader = {
      timeout = lib.mkForce 4;
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };
}
