{
  networking = {

    useDHCP = false;
    networkmanager.enable = true;
  };

  services = {
    avahi.nssmdns = false;
  };
}
