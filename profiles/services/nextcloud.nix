{ config, pkgs, ... }:
{

  security =
    {
      acme.acceptTerms = true;
      acme.defaults.email = "some@email";
    };
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud24;
    hostName = "delta";
    config = {
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      dbpassFile = "/etc/nixos/nextcloud-psql-pw";
      adminpassFile = "/etc/nixos/nextcloud-pw";
      adminuser = "admin";
    };
    # autoUpdateApps = true;
    maxUploadSize = "20G";
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
      {
        name = "nextcloud";
        ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
      }
    ];
  };

  # ensure that postgres is running *before* running the setup
  systemd.services."nextcloud-setup" = {
    requires = [ "postgresql.service" ];
    after = [ "postgresql.service" ];
  };

  services.nginx = {
    enable = true;
    virtualHosts = {
      "delta" = {
        forceSSL = true;
        enableACME = true;
      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
