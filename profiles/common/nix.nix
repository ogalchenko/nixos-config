{ pkgs, lib, inputs, config, ... }: {
  nix = rec {
    optimise.automatic = true;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';

    settings = {
      trusted-users = [ "root" "alex" "@wheel" ];
      substituters = [ "https://cache.nixos.org" ];
      auto-optimise-store = true;
    };

  };
}
