{ inputs, config, pkgs, ... }:
{


  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;

    users.alex = {

      home.stateVersion = "22.05";

      news.display = "silent";

      # Start new or changed services automatically.
      # Stop obsolte services from the previous generation
      systemd.user = {
        startServices = true;
      };
      programs = {
        home-manager = {
          enable = true;
        };

      };

    };

  };
}
  
