{
  services.xserver = {

    enable = true;

    displayManager.sddm.enable = true;
    desktopManager.plasma5.enable = true;

    layout = "us,ru,ua";
    libinput.enable = false;
    synaptics.enable = true; # enable touchpad on Legion 5 Pro

  };
}
