{ inputs, config, pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfree = true;

  imports = with inputs.self.nixosProfiles;
    [
      # Common
      nix
      # Services
      nextcloud
      networking
      # Hardware
      boot
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.kernelModules = [ ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Kiev";

  networking.hostName = "delta";
  services.nextcloud.hostName = "delta";

  hardware.cpu.intel.updateMicrocode = true;
  hardware.enableRedistributableFirmware = true;
  hardware.enableAllFirmware = true;

  users.users.alex = {
    initialPassword = "12345";
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "audio" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

  environment.systemPackages = with pkgs;
    [
      vim
      wget
      htop
      mc
    ];

  system.stateVersion = "21.11"; # Did you read the comment?

}

