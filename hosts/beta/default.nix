{ inputs, config, pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfree = true;

  imports = with inputs.self.nixosProfiles;
    [
      # Common
      inputs.home-manager.nixosModules.home-manager
      home
      nix
      # Services
      power
      networking
      tor
      # Hardware
      boot
      bolt
      ./hardware-configuration.nix
      sound
      # Workspace
      firefox
      zsh
      plasma
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.kernelModules = [ "uvcvideo" ];
    blacklistedKernelModules = [ "elan_i2c" "elants_i2c" "psmouse"];
  };

  # Set your time zone.
  time.timeZone = "Europe/Kiev";

  networking.hostName = "beta";

  hardware.cpu.intel.updateMicrocode = true;
  hardware.enableRedistributableFirmware = true;
  hardware.enableAllFirmware = true;

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.enable = true;

  users.users.alex = {
    initialPassword = "12345";
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "audio" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

  programs.steam.enable = true;

  environment.systemPackages = with pkgs;
    [
      neochat
      irssi
      chromium
      kget
      ktorrent
      exa
      fzf
      bat
      entr
      vim
      wget
      htop
      mc
      git
      krdc
      neofetch
      teams
      nixpkgs-fmt
      mesa-demos
      vulkan-tools
      emacs
      inxi
      libwebcam
      rxvt-unicode
      rnix-lsp
      gcc
      networkmanagerapplet
      rustup
      tdesktop
      openconnect
      kate
      gping
      mpv
      ripgrep
      libreoffice
    ];

  fonts.fonts = with pkgs ; [ fira fira-mono fira-code ];
  system.stateVersion = "21.11"; # Did you read the comment?

}

