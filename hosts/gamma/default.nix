{ inputs, config, pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfree = true;

  imports = with inputs.self.nixosProfiles;
    [
      # Common
      nix
      # Services
      networking
      # Hardware
      boot
      ./hardware-configuration.nix
      sound
      # Workspace
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.kernelModules = [ ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Kiev";

  networking.hostName = "gamma";

  hardware.cpu.intel.updateMicrocode = true;
  hardware.enableRedistributableFirmware = true;
  hardware.enableAllFirmware = true;

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.enable = true;


  services.xserver.enable = true;
  services.xserver.desktopManager.kodi.enable = true;
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "kodi";

  # Define a user account
  users.extraUsers.kodi = {
    isNormalUser = true;
    initialPassword = "12345";
  };

  networking.firewall = {
    allowedTCPPorts = [ 8080 ];
    allowedUDPPorts = [ 8080 ];
  };

  users.users.alex = {
    initialPassword = "12345";
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "audio" ]; # Enable ‘sudo’ for the user.
  };

  environment.systemPackages = with pkgs;
    [
      vim
      wget
      htop
      mc
      mesa-demos
      vulkan-tools
    ];

  system.stateVersion = "21.11"; # Did you read the comment?

}

