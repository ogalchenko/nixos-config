{ inputs, config, pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfree = true;

  imports = with inputs.self.nixosProfiles;
    [
      # Common
      inputs.home-manager.nixosModules.home-manager
      home
      nix
      # Services
      power
      networking
      tor
      # Hardware
      boot
      ./hardware-configuration.nix
      nvidia
      sound
      # Workspace
      firefox
      zsh
      plasma
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    initrd.kernelModules = [ "amdgpu" "uvcvideo" ];
    blacklistedKernelModules = [
      "nouveau"
      "rivafb"
      #     "nvidiafb"
      "rivatv"
      "nv"
      "uvcvideo"
    ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Kiev";

  networking.hostName = "alpha";
  hardware.bluetooth.enable = true;

  # tmp hack
  # programs.ssh.askPassword = pkgs.lib.mkForce "${pkgs.plasma5.ksshaskpass.out}/bin/ksshaskpass";
  # programs.ssh.askPassword = pkgs.lib.mkForce "${pkgs.gnome.seahorse.out}/libexec/seahorse/ssh-askpass";
  # services.xserver.desktopManager.gnome.enable = true;
  #services.xserver.enable = true;
  #services.xserver.displayManager.gdm.enable = true;
  #services.xserver.desktopManager.gnome.enable = true;
  hardware.cpu.amd.updateMicrocode = true;

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.enable = true;

  users.users.alex = {
    initialPassword = "12345";
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "audio" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

  programs.steam.enable = true;

  environment.systemPackages = with pkgs;
    [
      freetype
      ark

      gparted
      #      nvtop
      #     irssi
      #     konversation
      #      neochat
      #      weechat
      #      weechatScripts.weechat-matrix
      #      nheko
      #      fractal

      thunderbird
      chromium
      discord
      # FOR GNOME ONLY!
      #gnome-photos
      #gnomeExtensions.appindicator
      #gnome.cheese # webcam tool
      #gnome.gnome-music
      #gnome.gnome-terminal
      #gnome.gedit # text editor
      #gnome.epiphany # web browser
      #gnome.geary # email reader
      #gnome.evince # document viewer
      #gnome.gnome-characters
      #gnome.gnome-tweaks
      # FOR GNOME ONLY!
      xorg.libX11.dev
      nim
      nimlsp
      nimble-unwrapped
      xorg.libXft
      xorg.libXinerama

      kget
      ktorrent
      #transmission
      #exa
      #fzf
      #bat
      #entr
      vim
      wget
      htop
      mc
      git
      krdc
      #remmina
      neofetch
      teams
      nixpkgs-fmt
      mesa-demos
      vulkan-tools
      emacs
      inxi
      libwebcam
      #rxvt-unicode
      rnix-lsp
      gcc
      #networkmanagerapplet
      #rustup
      #rls
      #rustfmt
      tdesktop
      openconnect
      kate
      gping
      mpv
      ripgrep
      libreoffice
      (fenix.complete.withComponents [
        "cargo"
        "clippy"
        "rust-src"
        "rustc"
        "rustfmt"
      ])
      rust-analyzer-nightly
    ];

  fonts = {

    fontconfig = {
      enable = true;
      antialias = true;
      hinting.enable = true;
      hinting.autohint = true;
      subpixel.lcdfilter = "default";
    };

    fonts = with pkgs ; [
      noto-fonts
      proggyfonts
      roboto-mono

      anonymousPro


      fira
      fira-mono
      fira-code

      source-code-pro
      cascadia-code
      jetbrains-mono

    ];

  };

  system.stateVersion = "22.05"; # Did you read the comment?

}

